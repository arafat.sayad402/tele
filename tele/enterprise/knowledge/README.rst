=========
Knowledge
=========

.. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !! This file is generated by oca-gen-addon-readme !!
   !! changes will be overwritten.                   !!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

.. |badge1| image:: https://img.shields.io/badge/maturity-Beta-yellow.png
    :target: https://tele-community.org/page/development-status
    :alt: Beta
.. |badge2| image:: https://img.shields.io/badge/licence-AGPL--3-blue.png
    :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
    :alt: License: AGPL-3
.. |badge3| image:: https://img.shields.io/badge/github-OCA%2Fknowledge-lightgray.png?logo=github
    :target: https://github.com/tele-studio/tele/tree/1.0/knowledge
    :alt: tele-studio/tele
.. |badge4| image:: https://img.shields.io/badge/weblate-Translate%20me-F47D42.png
    :target: https://translation.tele-community.org/projects/knowledge-15-0/knowledge-15-0-knowledge
    :alt: Translate me on Weblate
.. |badge5| image:: https://img.shields.io/badge/runbot-Try%20me-136dc7.png
    :target: https://runbot.tele-community.org/runbot/118/15.0
    :alt: Try me on Runbot

|badge1| |badge2| |badge3| |badge4| |badge5| 

This module is the base for any knowledge and document management application.

**Table of contents**

.. contents::
   :local:

Configuration
=============

To set up this module, you need to go to:

* Knowledge / Configuration / Settings

From this menu you'll have a central access to install the apps that belong
to Knowledge.

* Check *Attachments List and Document Indexation* if you want to install the
  module that allows users to attach documents to any model.
* Check *Manage attachments centrally* if you want all users to be able to
  access to the all attachments to which they have read permissions, from the
  menu *Knowledge / Documents*

If you want to grant Central Access to Documents only to some users:

#. Go to *Settings/Activate the developer mode*. Only a user with
   *Administration / Settings* permissions can do that.

#. Go to *Settings / Users & Companies / Users* and set the checkbox
   *Central access to Documents* to the selected users.

Usage
=====

This module adds a new top level menu *Knowledge*

Users with permission *Central access to Documents* can access in
*Knowledge/Documents* to all the documents attached to records of any model
for which they have read permission.

Bug Tracker
===========

Bugs are tracked on `GitHub Issues <https://github.com/tele-studio/tele/issues>`_.
In case of trouble, please check there if your issue has already been reported.
If you spotted it first, help us smashing it by providing a detailed and welcomed
`feedback <https://github.com/tele-studio/tele/issues/new?body=module:%20knowledge%0Aversion:%2015.0%0A%0A**Steps%20to%20reproduce**%0A-%20...%0A%0A**Current%20behavior**%0A%0A**Expected%20behavior**>`_.

Do not contact contributors directly about support or help with technical issues.

Credits
=======

Authors
~~~~~~~

* OpenERP SA
* MONK Software
* Tecnativa
* ForgeFlow

Contributors
~~~~~~~~~~~~

* Tele INC <info@tele.studio>
* Savoir-faire Linux <support@savoirfairelinux.com>
* Gervais Naoussi <gervaisnaoussi@gmail.com>
* Leonardo Donelli <leonardo.donelli@monksoftware.it>
* Maxime Chambreuil <mchambreuil@ursainfosystems.com>
* Fayez Qandeel
* Iván Todorovich <ivan.todorovich@gmail.com>
* Jordi Ballester <jordi.ballester@forgeflow.com>
* `Tecnativa <https://www.tecnativa.com>`_:

  * Vicent Cubells
  * Ernesto Tejeda

Trobz

* Dung Tran <dungtd@trobz.com>

Other credits
~~~~~~~~~~~~~

The development of this module has been financially supported by:

* Camptocamp

Maintainers
~~~~~~~~~~~

This module is maintained by the OCA.

.. image:: https://tele-community.org/logo.png
   :alt: Tele-Community Association
   :target: https://tele-community.org

OCA, or the Tele-Community Association, is a nonprofit organization whose
mission is to support the collaborative development of Tele features and
promote its widespread use.

This module is part of the `tele-studio/tele <https://github.com/tele-studio/tele/tree/1.0/knowledge>`_ project on GitHub.

You are welcome to contribute. To learn how please visit https://tele-community.org/page/Contribute.
